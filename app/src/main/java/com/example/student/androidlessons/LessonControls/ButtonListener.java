package com.example.student.androidlessons.LessonControls;


import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

public class ButtonListener implements View.OnClickListener {

    private TextView textView;

    private String text = "Button clicked!";

    public ButtonListener(TextView textView) {
        this.textView = textView;
    }

    public ButtonListener(TextView textView, String text) {
        this.textView = textView;
        this.text = text;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        textView.setText(this.text);
    }
}
