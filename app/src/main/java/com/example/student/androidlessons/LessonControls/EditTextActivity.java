package com.example.student.androidlessons.LessonControls;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.example.student.androidlessons.R;

public class EditTextActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_edit_text);
        setContentView(getEditTextLayout());
    }

    public View getEditTextLayout() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        EditText loginEditText = new EditText(this);
        loginEditText.setHint("Enter your login");
        loginEditText.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_FLAG_MULTI_LINE);

        EditText passwordEditText = new EditText(this);
        passwordEditText.setHint("Enter your password");
        passwordEditText.setInputType(InputType.TYPE_CLASS_TEXT|InputType.TYPE_TEXT_VARIATION_PASSWORD);

        layout.addView(loginEditText);
        layout.addView(passwordEditText);

        return layout;
    }
}
