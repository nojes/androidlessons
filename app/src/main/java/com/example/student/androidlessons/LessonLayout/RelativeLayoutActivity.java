package com.example.student.androidlessons.LessonLayout;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class RelativeLayoutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.relative_layout_lesson);
//        setContentView(getRelativeLayout());
        setContentView(getRelativeLayoutTask1());
    }

    @SuppressLint("SetTextI18n")
    public View getRelativeLayout()
    {
        RelativeLayout layout = new RelativeLayout(this);
        layout.setMinimumHeight(300);
        layout.setMinimumWidth(300);

        TextView helloText = new TextView(this);
        helloText.setText("Hello");
        RelativeLayout.LayoutParams helloTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        helloTextParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        layout.addView(helloText, helloTextParams);

        TextView worldText = new TextView(this);
        worldText.setText("World");
        RelativeLayout.LayoutParams helloText2Params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        helloText2Params.addRule(RelativeLayout.BELOW, helloText.getId());
        helloText2Params.addRule(RelativeLayout.ALIGN_RIGHT, helloText.getId());
        layout.addView(worldText, helloText2Params);

        return layout;
    }

    public View getRelativeLayoutTask1() {
        RelativeLayout layout = new RelativeLayout(this);
        layout.setMinimumHeight(300);
        layout.setMinimumWidth(300);

        TextView text1 = new TextView(this);
        text1.setText("text1");
        RelativeLayout.LayoutParams helloTextParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        helloTextParams.addRule(RelativeLayout.CENTER_IN_PARENT);
        layout.addView(text1, helloTextParams);

        return layout;
    }
}
