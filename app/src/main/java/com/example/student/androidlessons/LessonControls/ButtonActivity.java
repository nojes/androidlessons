package com.example.student.androidlessons.LessonControls;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class ButtonActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_button);
//        setContentView(getButtonsLayout());
        setContentView(getButtonsLayout2());
    }

    @SuppressLint("SetTextI18n")
    public void buttonLayout(View sender) {
        Button btn = (Button)sender;
        btn.setText("Done");
    }

    public View getButtonsLayout() {
        LinearLayout layout = new LinearLayout(this);
        layout.setOrientation(LinearLayout.VERTICAL);

        Button button1 = new Button(this);
        button1.setText("Button1");

        final TextView textView = new TextView(this);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                textView.setText("Button1 click!");
            }
        });

        layout.addView(button1);
        layout.addView(textView);

        Button button2 = new Button(this);
        button2.setText("Button2");
        TextView textView2 = new TextView(this);

        ButtonListener listener = new ButtonListener(textView2);
        button2.setOnClickListener(listener);

        layout.addView(button2);
        layout.addView(textView2);

        return layout;
    }

    @SuppressLint("SetTextI18n")
    public View getButtonsLayout2() {
        RelativeLayout layout = new RelativeLayout(this);
        layout.setMinimumHeight(300);
        layout.setMinimumWidth(300);

        Button button1 = new Button(this);
        button1.setText("button1");
        RelativeLayout.LayoutParams button1Params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        button1Params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        button1Params.addRule(RelativeLayout.CENTER_VERTICAL);

        TextView textView1 = new TextView(this);
        RelativeLayout.LayoutParams textView1Params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView1Params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        textView1Params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        Button button2 = new Button(this);
        button2.setText("button2");
        RelativeLayout.LayoutParams button2Params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        button2Params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        button2Params.addRule(RelativeLayout.CENTER_VERTICAL);

        TextView textView2 = new TextView(this);
        RelativeLayout.LayoutParams textView2Params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        textView2Params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        textView2Params.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);

        ButtonListener listener1 = new ButtonListener(textView1, "Hello1");
        button1.setOnClickListener(listener1);

        ButtonListener listener2 = new ButtonListener(textView2, "Hello2");
        button2.setOnClickListener(listener2);

        layout.addView(button1, button1Params);
        layout.addView(button2, button2Params);
        layout.addView(textView1, textView1Params);
        layout.addView(textView2, textView2Params);

        return layout;
    }

}
