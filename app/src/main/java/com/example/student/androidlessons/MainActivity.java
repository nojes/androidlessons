package com.example.student.androidlessons;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.student.androidlessons.LessonControls.DatePickerActivity;
import com.example.student.androidlessons.LessonControls.ImageViewActivity;
import com.example.student.androidlessons.LessonControls.TimePickerActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Button btnTimePicker = (Button)findViewById(R.id.btnTimePicker);
        Button btnDatePicker = (Button)findViewById(R.id.btnDatePicker);
        Button btnImageView = (Button)findViewById(R.id.btnImageView);

        final MainActivity me = this;

        btnTimePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity= new Intent(me, TimePickerActivity.class);
                startActivity(activity);
            }
        });

        btnDatePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity= new Intent(me, DatePickerActivity.class);
                startActivity(activity);
            }
        });

        btnImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activity= new Intent(me, ImageViewActivity.class);
                startActivity(activity);
            }
        });
    }
}
