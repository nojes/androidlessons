package com.example.student.androidlessons.LessonControls;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.example.student.androidlessons.R;

public class DatePickerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_date_picker);
    }

    public void onGetDate(View view) {
        DatePicker datePicker = (DatePicker)findViewById(R.id.datePicker);
        TextView txtGetResult = (TextView)findViewById(R.id.txtDateResult);

        StringBuilder result = new StringBuilder();

        result.append("Year: ").append(datePicker.getYear()).append(" / ");
        result.append("Month: ").append(datePicker.getMonth()).append(" / ");
        result.append("Day: ").append(datePicker.getDayOfMonth()).append(" / ");

        txtGetResult.setText(result);
    }
}
